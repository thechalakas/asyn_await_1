﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace asyn_await_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //here, the program knows that the demo_async_await is async
            //so the compiler treats it as such
            string result_of_web_request = demo_async_await().Result;
            Console.WriteLine(result_of_web_request);

            Console.ReadLine();
        }

        //await can be used in an async method only
        //I cannot make the main method async 
        //so from main, I will call this async method which will in turn call an await method
        static async Task<string> demo_async_await()
        {
            using (HttpClient client = new HttpClient())
            {
                //the await keyword indicates that this activity will take a while
                //if the following is done, then the code just continues
                //if the following is done, it will take a while, in which case, control is returned
                //to the main thread which can do other stuff
                string result = await client.GetStringAsync("http://www.studynildana.com");
                return result;
            }
        }
    }
}
